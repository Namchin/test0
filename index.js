const CONFIG = {
dbUri: 'mongodb://127.0.0.1/livercenter',
fhirServer: 'http://10.0.1.108:3335',
backend: 'http://10.0.1.108:3336',
frontend: 'http://10.0.1.108:7000',
LAB_ELEG_MN: 'http://103.17.108.89',
lab_eleg_mn_SMS_API: 'http://103.17.108.89:3300/api/v1/sendSMS',
secret: 'msonfhir',
LABORATORY_REPORT_OUTPUT_DIR: 'laboratory-reports',
EXCEL_OUTPUT_DIR: 'excel-documents',
API: {
ICD: {
token_access_path: 'https://icdaccessmanagement.who.int/connect/token',
client_id:
'3431c747-511b-452e-ae22-db7f150c7b97_cd6afa62-ef5b-4ba7-9aa8-614d73811c3e',
client_secret: 'Y3DpslqXExneQTykmZo/AcKO1sxmpOaXLW3Jo06M8kQ=',
},
EPRESCRIPTION: {
url:
'https://st.auth.itc.gov.mn/auth/realms/Staging/protocol/openid-connect/token',
prescription_url:
'https://st.health.gov.mn/api/v2/service/service/getTabletByGroup',
citizenInfo_url:
'https://st.health.gov.mn/api/v2/consumer/consumer/getCitizenInfo',
getNoFinger_url:
'https://st.health.gov.mn/api/v2/consumer/consumer/getNoFinger',
savePrescription_url:
'https://st.health.gov.mn/api/v2/prescription/prescription/savePrescription',
savedPrescriptionByType_url:
'https://st.health.gov.mn/api/v2/prescription/prescription/getReceiptByType',
drugInfoByDiagnose_url:
'https://st.health.gov.mn/api/v2/prescription/diagnosis/getTabletByDiagnosis',
data: {
client_id: 'health',
grant_type: 'password',
username: 'hospital_test1',
password: 'hospital1',
},
},
},
CERTIFICATE: {
DEMO_LIVERCENTER_MN: {
privkey: '/etc/letsencrypt/live/demo.livercenter.mn/privkey.pem',
fullchain: '/etc/letsencrypt/live/demo.livercenter.mn/fullchain.pem',
},
APP_LIVERCENTER_MN: {
privkey: '/etc/letsencrypt/live/app.livercenter.mn/privkey.pem',
fullchain: '/etc/letsencrypt/live/app.livercenter.mn/fullchain.pem',
},
},
ROLE_TYPE: {
ADMIN: 'admin',
DEFAULT: 'admin',
DEVELOPER: 'developer',
LABORATORY_TECHNICIAN: 'laboratory-technician',
SENIOR_LABORATORY_TECHNICIAN: 'senior-laboratory-technician',
PHLEBOTOMIST: 'phlebotomist',
RECEPTIONIST: 'receptionist',
PRACTITIONER: 'practitioner',
PATIENT: 'patient',
},
STATUS: {
EXTERNAL_SPECIMEN_LOG: {
PENDING: 'pending',
CANCELLED: 'cancelled',
CONFIRMED: 'confirmed',
},
},
PAGE_SIZE: 20,
PAGE_NUMBER: 1,
}
module.exports = CONFIG
